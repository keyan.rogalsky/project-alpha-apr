from django.forms import ModelForm
from projects.models import Project


# form model for new projects
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )
