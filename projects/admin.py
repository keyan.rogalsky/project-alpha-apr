from django.contrib import admin
from projects.models import Project


# registers model
@admin.register(Project)
class ProjectsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )
